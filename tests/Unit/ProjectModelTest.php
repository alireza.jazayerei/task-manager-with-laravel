<?php

namespace Tests\Unit;

use App\Project;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProjectModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function project_should_have_path()
    {
        $project = factory(Project::class)->create();
        $this->assertEquals("/projects/" . $project->id, $project->path());
    }

    /** @test */
    public function project_has_a_user()
    {
        $project = factory(Project::class)->create();
        $this->assertInstanceOf(User::class, $project->owner);
    }

    /** @test */
    public function it_can_add_task()
    {
        $project = factory(Project::class)->create();
        $task = $project->addTask("task test");
        $this->assertCount(1, $project->tasks);
        $this->assertTrue($project->tasks->contains($task));
    }

    /** @test */
    public function project_can_have_activity()
    {
        $project = factory(Project::class)->create();
        $this->assertInstanceOf(Collection::class, $project->activity);
    }

    /** @test */
    public function it_can_invite_users()
    {
        $project = factory(Project::class)->create();
        $project->invite($user=factory(User::class)->create());
        $this->assertTrue($project->members->contains($user));
    }
}
