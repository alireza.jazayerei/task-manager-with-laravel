<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Arranges\ProjectFactory;
use Tests\TestCase;

class ActivityModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_a_user()
    {
        $project = ProjectFactory::create();
        $this->actingAs($project->owner);
        $this->assertInstanceOf(User::class, $project->activity->first()->user);
        $this->assertEquals($project->owner->id, $project->activity->first()->user->id);
    }
}
