<?php

namespace Tests\Unit;

use App\Project;
use App\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TaskModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function task_have_path()
    {
        $task = factory(Task::class)->create();
        $this->assertEquals($task->project->path() . "/tasks/" . $task->id, $task->path());
    }

    /** @test */
    public function task_has_project()
    {
        $task = factory(Task::class)->create();
        $this->assertInstanceOf(Project::class, $task->project);
    }

    /** @test */
    public function it_can_be_completed()
    {
        $task = factory(Task::class)->create();
        $this->assertFalse($task->completed);
        $task->complete();
        $this->assertTrue($task->completed);
    }

    /** @test */
    public function it_can_be_incompleted()
    {
        $task = factory(Task::class)->create(["completed" => true]);
        $this->assertTrue($task->completed);
        $task->incomplete();
        $this->assertFalse($task->completed);

    }
}
