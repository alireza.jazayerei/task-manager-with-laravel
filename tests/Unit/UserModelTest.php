<?php

namespace Tests\Unit;

use App\Project;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_has_projects()
    {
        $user = factory(User::class)->create();
        $this->assertInstanceOf(Collection::class, $user->projects);
    }

    /** @test */
    public function it_can_see_all_projects_he_owns_and_is_a_member_of()
    {
        $john = factory(User::class)->create();
        factory(Project::class)->create(["owner_id" => $john->id]);

        $this->assertCount(1, $john->allProjects());

        $sally = factory(User::class)->create();
        $nick = factory(User::class)->create();

        $sallyProject = factory(Project::class)->create(["owner_id" => $sally->id]);
        $sallyProject->invite($nick);

        $this->assertCount(1, $john->allProjects());

        $sallyProject->invite($john);

        $this->assertCount(2, $john->allProjects());
    }
}
