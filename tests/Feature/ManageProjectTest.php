<?php

namespace Tests\Feature;

use App\Project;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Facades\Tests\Arranges\ProjectFactory;
use Tests\TestCase;

class ManageProjectTest extends TestCase
{
    use RefreshDatabase, withFaker;

    /** @test */
    public function guests_cannot_manage_projects()
    {
        $project = factory(Project::class)->create();
        $this->get($project->path())->assertRedirect("/login");

        $this->get('/projects')->assertRedirect("/login");

        $attributes = factory(Project::class)->raw(["owner_id" => null]);
        $this->post('/projects', $attributes)->assertRedirect("/login");
    }

    /**  @test */
    public function a_user_can_create_a_project()
    {
        $this->signIn();

        $this->get("/projects/create")->assertStatus(200);

        $this->followingRedirects()
            ->post('/projects', $attributes = factory(Project::class)->raw(["owner_id" => auth()->user()->id]))
            ->assertSee($attributes["title"])->assertSee($attributes["note"]);

        $this->assertDatabaseHas("projects", $attributes);
    }

    /** @test */
    public function title_is_required()
    {
        $this->signIn();
        $attributes = factory(Project::class)->raw(["title" => ""]);
        $this->post('/projects', $attributes)->assertSessionHasErrors("title");
    }

    /** @test */
    public function description_is_required()
    {
        $this->signIn();
        $attributes = factory(Project::class)->raw(["description" => ""]);
        $this->post('/projects', $attributes)->assertSessionHasErrors("description");
    }

    /** @test */
    public function a_user_can_view_their_project()
    {
        $project = ProjectFactory::create();
        $this->actingAs($project->owner)
            ->get($project->path())
            ->assertSee($project->title)
            ->assertSee($project->description);

    }

    /** @test */
    public function an_authenticated_user_cannot_see_other_users_projects()
    {
        $this->signIn();
        $project = factory(Project::class)->create();
        $this->get($project->path())->assertStatus(403);
    }

    /** @test */
    public function note_should_be_at_least_3_characters()
    {
        $this->signIn();
        $attributes = factory(Project::class)->raw(["note" => "as"]);
        $this->post('/projects', $attributes)->assertSessionHasErrors("note");
    }

    /** @test */
    public function a_project_can_be_updated()
    {
        $project = ProjectFactory::create();
        $attributes = ["note" => "project note"];
        $this->actingAs($project->owner)
            ->patch($project->path(), $attributes);
        $this->assertDatabaseHas("projects", $attributes);
    }

    /** @test */
    public function only_owner_can_update_project()
    {
        $this->signIn();
        $project = factory(Project::class)->create();
        $attributes = ["note" => "project note to authorize"];
        $this->patch($project->path(), $attributes)->assertStatus(403);
        $this->assertDatabaseMissing("projects", $attributes);
    }

    /** @test */
    public function projects_can_be_deleted()
    {
        $project = ProjectFactory::create();
        $this->actingAs($project->owner)
            ->delete($project->path());
        $this->assertDatabaseMissing("projects", $project->only("id"));
    }

    /** @test */
    public function unauthorized_users_cannot_delete_projects()
    {
        $project = ProjectFactory::create();
        $this->delete($project->path())->assertRedirect("/login");
        $user=$this->signIn();
        $this->delete($project->path())->assertStatus(403);
        $project->invite($user);
        $this->delete($project->path())->assertStatus(403);
    }

    /** @test */
    public function user_can_see_the_projects_that_he_is_invited_to_at_the_dashboard()
    {
        $this->signIn($john = factory(User::class)->create());
        $project = tap(ProjectFactory::create())->invite($john);
        $this->get("/projects")->assertSee($project->title);
    }


}
