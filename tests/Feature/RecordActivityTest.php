<?php

namespace Tests\Feature;

use App\Project;
use App\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Arranges\ProjectFactory;
use Tests\TestCase;

class RecordActivityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function creating_a_project()
    {
        $this->withExceptionHandling();
        $project = ProjectFactory::create();
        $this->assertCount(1, $project->activity);
        $this->assertEquals("created_project", $project->activity->first()->description);
        $this->assertNull($project->activity->first()->changes);
    }

    /** @test */
    public function updating_a_project()
    {
        $this->withExceptionHandling();
        $project = ProjectFactory::create();
        $originalTitle = $project->title;
        $project->update([
            "title" => "changed",
        ]);
        $expected = [
            'before' => ["title" => $originalTitle],
            'after' => ["title" => "changed"],
        ];

        $this->assertCount(2, $project->activity);
        tap($project->activity->last(), function ($activity) use ($expected) {
            $this->assertEquals($expected, $activity->changes);
            $this->assertEquals("updated_project", $activity->description);
        });

    }

    /** @test */
    public function creating_task()
    {
        $project = ProjectFactory::create();
        $project->addTask("some task");
        $this->assertCount(2, $project->activity);
        tap($project->activity->last(), function ($activity) {
            $this->assertEquals("created_task", $activity->description);
            $this->assertInstanceOf(Task::class, $activity->subject);
            $this->assertEquals("some task", $activity->subject->body);
        });


    }

    /** @test */
    public function completing_a_task()
    {
        $project = ProjectFactory::withTask(1)->create();
        $this->actingAs($project->owner)
            ->post("/complete-task/" . $project->tasks->first()->id);
        $this->assertCount(3, $project->activity);
    }

    /** @test */
    public function incompleting_a_task()
    {
        $project = ProjectFactory::withTask(1)->create();
        $this->actingAs($project->owner)
            ->post("complete-task/" . $project->tasks->first()->id);
        $this->assertCount(3, $project->activity);
        $this->delete("complete-task/" . $project->tasks->first()->id);
        $this->assertCount(4, $project->fresh()->activity);
        $this->assertEquals("incompleted_task", $project->fresh()->activity->last()->description);
    }

    /** @test */
    public function deleting_a_task()
    {
        $project = ProjectFactory::withTask(1)->create();
        $this->actingAs($project->owner);
        $project->tasks->first()->delete();
        $this->assertCount(3, $project->activity);
        $this->assertEquals("deleted_task", $project->fresh()->activity->last()->description);
    }
}
