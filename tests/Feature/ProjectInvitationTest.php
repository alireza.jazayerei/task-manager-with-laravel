<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Arranges\ProjectFactory;
use phpDocumentor\Reflection\Project;
use Tests\TestCase;

class ProjectInvitationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function invited_users_may_update_project_details()
    {
        $project = ProjectFactory::create();
        $project->invite($newUser = factory(User::class)->create());
        $this->signIn($newUser);
        $this->post($project->path() . "/tasks", $task = ["body" => "hi every body"]);
        $this->assertDatabaseHas("tasks", $task);
    }

    /** @test */
    public function a_project_can_invite_users()
    {
        $this->withExceptionHandling();
        $project = ProjectFactory::create();
        $userToInvite = factory(User::class)->create();
        $this->actingAs($project->owner)
            ->post($project->path() . "/invitation", [
                "email" => $userToInvite->email
            ])->assertRedirect($project->path());
        $this->assertTrue($project->members->contains($userToInvite));

    }

    /** @test */
    public function entered_email_address_should_belong_to_an_available_user_of_the_site()
    {
        $project = ProjectFactory::create();
        $this->actingAs($project->owner)
            ->post($project->path() . "/invitation", [
                "email" => "myemail@gmail.com",
            ])
            ->assertSessionHasErrors([
                "email" => "this email does not belong to a user in site",
            ]);
    }

    /** @test */
    public function only_owner_of_project_can_invite()
    {
        $project = ProjectFactory::create();
        $user = factory(User::class)->create();

        $assertUserCannotInvite = function () use ($project, $user) {
            $this->actingAs($user)
                ->post($project->path() . "/invitation")
                ->assertStatus(403);
        };

        $assertUserCannotInvite();

        $project->invite($user);

        $assertUserCannotInvite();
    }
}
