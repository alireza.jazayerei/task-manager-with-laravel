<?php

namespace Tests\Feature;

use App\Project;
use App\Task;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Arranges\ProjectFactory;
use Tests\TestCase;

class ProjectTasksTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guests_cannot_add_task()
    {
        $project = factory(Project::class)->create();
        $this->post($project->path() . "/tasks")->assertRedirect("/login");
    }

    /** @test */
    public function project_can_have_tasks()
    {
        //arrange
        $project = ProjectFactory::create();
        //act
        $this->actingAs($project->owner)
            //assert
            ->post($project->path() . "/tasks", ["body" => "test task"]);
        $this->get($project->path())->assertSee("test task");
    }

    /** @test */
    public function a_task_requires_body()
    {
        $project = ProjectFactory::create();
        $attributes = factory(Task::class)->raw(["body" => ""]);
        $this->actingAs($project->owner)
            ->post($project->path() . "/tasks", $attributes)
            ->assertSessionHasErrors("body");
    }

    /** @test */
    public function only_owner_can_add_task()
    {
        $this->signIn();
        $project = ProjectFactory::create();
        $attributes = ["body" => "test authorize"];
        $this->post($project->path() . "/tasks", $attributes)->assertStatus(403);
        $this->assertDatabaseMissing("tasks", $attributes);

    }

    /** @test */
    public function a_task_can_be_updated()
    {

        $project = ProjectFactory::withTask(1)->create();
        $attributes = [
            "body" => "changed",
        ];
        $this->actingAs($project->owner)
            ->patch($project->tasks->first()->path(), $attributes);
        $this->assertDatabaseHas("tasks", $attributes);
    }

    /** @test */
    public function only_projects_owner_can_update_the_task()
    {
        $this->signIn();
        $project = ProjectFactory::withTask(1)->create();
        $attributes = [
            "body" => "changed to meee",
            "completed" => true,
        ];
        $this->patch($project->tasks->first()->path(), $attributes)->assertStatus(403);
        $this->assertDatabaseMissing("tasks", $attributes);
    }

    /** @test */
    public function body_is_required_to_update_task()
    {
        $project = ProjectFactory::withTask(1)->create();
        $attributes = [
            "body" => "",
            "completed" => true,
        ];
        $this->actingAs($project->owner)
            ->patch($project->tasks->first()->path(), $attributes)
            ->assertSessionHasErrors("body");
    }

    /** @test */
    public function task_can_be_completed()
    {
        $project = ProjectFactory::withTask(1)->create();
        $this->actingAs($project->owner);
        $this->post("complete-task/" . $project->tasks->first()->id);
        $this->assertDatabaseHas("tasks", ["completed" => 1]);
    }

    /** @test */
    public function task_can_be_incompleted()
    {
        $project = ProjectFactory::withTask(1)->create();
        $this->actingAs($project->owner);
        $this->post("complete-task/" . $project->tasks->first()->id);
        $this->assertDatabaseHas("tasks", ["completed" => 1]);
        $this->delete("complete-task/" . $project->tasks->first()->id);
        $this->assertDatabaseHas("tasks", ["completed" => 0]);
    }

    /** @test */
    public function task_can_be_deleted()
    {
        $project = ProjectFactory::withTask(1)->create();
        $this->actingAs($project->owner)
            ->delete($project->tasks->first()->path());
        $this->assertDatabaseMissing("tasks", ["body" => $project->tasks->first()->body]);
    }

    /** @test */
    public function only_project_owner_can_delete_their_task()
    {
        $project = ProjectFactory::withTask(1)->create();
        $this->actingAs(factory(User::class)->create())
            ->delete($project->tasks->first()->path())
            ->assertStatus(403);
    }
}
