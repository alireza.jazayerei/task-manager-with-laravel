<?php


namespace Tests\Arranges;


use App\Project;
use App\Task;
use App\User;

class ProjectFactory
{
    private $taskCount=0;

    public function create()
    {
        $project = factory(Project::class)->create([
            "owner_id" => factory(User::class),
        ]);

        factory(Task::class, $this->taskCount)->create([
            "project_id" => $project->id,
        ]);

        return $project;
    }

    public function withTask($taskCount = 1)
    {
        $this->taskCount = $taskCount;
        return $this;
    }
}
