<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Http\Controllers\ProjectInvitationsController;

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(["auth"])->group(function () {
    Route::post('/projects', "ProjectsController@store")->name("projects.store");
    Route::get('/projects', "ProjectsController@index")->name("projects.index");
    Route::get('/projects/create', "ProjectsController@create")->name("projects.create");
    Route::get('/projects/{project}', "ProjectsController@show")->name("projects.show");
    Route::patch('/projects/{project}', "ProjectsController@update")->name("Projects.update");
    Route::delete("/projects/{project}", "ProjectsController@destroy")->name("Projects.destroy");

    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/projects/{project}/tasks', "ProjectTasksController@store")->name("ProjectTasks . store");
    Route::patch('/projects/{project}/tasks/{task}', "ProjectTasksController@update")->name("ProjectTasks . update");
    Route::delete('/projects/{project}/tasks/{task}', "ProjectTasksController@destroy")->name("ProjectTasks . destroy");

    Route::post('/complete-task/{task}', "CompleteTaskController@store")->name("CompleteTask . store");
    Route::delete('/complete-task/{task}', "CompleteTaskController@destroy")->name("CompleteTask . destroy");

    Route::post('/projects/{project}/invitation', [ProjectInvitationsController::class,"store"])->name("ProjectInvitations.store");
});

Auth::routes();


