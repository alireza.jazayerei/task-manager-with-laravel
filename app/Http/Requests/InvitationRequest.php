<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvitationRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => ["exists:users,email"],
        ];
    }

    public function messages()
    {
        return [
            "email.exists" => "this email does not belong to a user in site",
        ];
    }
}
