<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class CompleteTaskController extends Controller
{
    public function store(Task $task)
    {
        $this->authorize("view", $task->project);
        $task->complete();
        return back();
    }

    public function destroy(Task $task)
    {
        $this->authorize("view", $task->project);
        $task->incomplete();
        return back();
    }
}
