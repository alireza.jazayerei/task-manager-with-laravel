<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvitationRequest;
use App\Project;
use App\User;

class ProjectInvitationsController extends Controller
{
    public function store(Project $project, InvitationRequest $request)
    {
        $this->authorize("manage", $project);
        $project->invite(User::where("email", $request->input("email"))->first());
        return redirect($project->path());
    }
}
