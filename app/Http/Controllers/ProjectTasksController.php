<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Project;
use App\Task;
use Illuminate\Http\Request;


class ProjectTasksController extends Controller
{
    public function store(Project $project, TaskRequest $request)
    {
        $this->authorize("view", $project);
        $project->addTask($request->input("body"));

        return redirect($project->path());
    }

    public function update(TaskRequest $request, Project $project, Task $task)
    {
        $this->authorize("view", $task->project);
        $task->update([
            "body" => $request->input("body"),
        ]);
        return redirect($project->path());
    }

    public function destroy(Project $project, Task $task)
    {
        $this->authorize("view", $task->project);
        $task->delete();
        return back();
    }

}
