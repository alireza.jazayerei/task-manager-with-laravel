<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function store(ProjectRequest $request)
    {
        $project = auth()->user()->projects()->create($request->all());
        return redirect($project->path());
    }

    public function index()
    {
        $projects = auth()->user()->AllProjects();
        return view("AllProjects", compact("projects"));
    }

    public function create()
    {
        return view("CreateProject");
    }

    public function show(Project $project)
    {
        $this->authorize("view", $project);
        return view("ShowProject", compact("project"));
    }

    public function update(Request $request, Project $project)
    {
        $this->authorize("view", $project);
        $project->update($request->all());
        return redirect($project->path());
    }

    public function destroy(Project $project)
    {
        $this->authorize("manage", $project);
        $project->delete();
        return redirect(route("projects.index"));
    }
}
