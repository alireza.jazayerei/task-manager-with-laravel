<?php


namespace App;


use Illuminate\Support\Arr;

trait RecordsActivity
{
    public $oldAttributes = [];

    public static function bootRecordsActivity()
    {

        foreach (self::recordableEvents() as $event) {
            static::$event(function ($model) use ($event) {

                $model->recordActivity($model->activityDescription($event));
            });
            if ($event === "updated") {
                static::updating(function ($model) {
                    $model->oldAttributes = $model->getOriginal();
                });
            }
        }
    }

    public static function recordableEvents()
    {
        if (isset(static::$recordableEvents)) {
            return static::$recordableEvents;
        }
        return ["created", "updated", "deleted"];
    }

    protected function recordActivity($description)
    {
        $this->activity()->create([
            "description" => $description,
            "changes" => $this->activityChanges(),
            "project_id" => class_basename($this) === "Project" ? $this->id : $this->project_id,
            "user_id" => ($this->project ?? $this)->owner->id,
        ]);
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, "subject");
    }

    private function activityChanges()
    {
        if ($this->wasChanged()) {
            return [
                'before' => arr::except(array_diff($this->oldAttributes, $this->getAttributes()), "updated_at"),
                'after' => arr::except($this->getChanges(), "updated_at"),
            ];
        }
        return null;
    }

    protected function activityDescription($event)
    {
        return $event . "_" . strtolower(class_basename($this));
    }
}
