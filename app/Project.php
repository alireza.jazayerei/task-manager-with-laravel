<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Project extends Model
{
    use recordsActivity;

    protected $table = 'projects';
    protected $fillable = ["title", "description", "owner_id", "note",];
    protected static $recordableEvents=["created","updated"];

    public function path()
    {
        return "/projects/" . $this->id;
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function addTask($body)
    {
        return $this->tasks()->create(compact("body"));
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }



    public function activity()
    {
        return $this->hasMany(Activity::class)->latest();
    }

    public function invite($user)
    {
        $this->members()->attach($user);
    }

    public function members()
    {
        return $this->belongsToMany(User::class,"project_members")->withTimestamps();
    }

}
