<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>{{$project->title}}</p>
<p>{{$project->description}}</p>
<p>{{$project->note}}</p>
<h1>Tasks</h1>
@foreach($project->tasks as $task)
    <p>{{$task->body}}</p>
@endforeach
</body>
</html>
